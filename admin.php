<?php

namespace sjr\tumblr_poster;

/**
*
*	@param string
*	@return
*/
function add_meta_boxes( $post_type ){
	$allowed = apply_filters( 'sjr\tumblr_poster\allowed_post_types', array('post') );

	if( in_array($post_type, $allowed) ){
		add_meta_box(
			'tumblr-crosspostr-meta-box',
			__('Tumblr Crosspostr', 'tumblr-crosspostr'),
			__NAMESPACE__.'\render_meta_box',
			$post_type,
			'side'
		);
	}
}
add_action( 'add_meta_boxes', __NAMESPACE__.'\add_meta_boxes' );

/**
*
*/
function authorize_app(){
	check_admin_referer( 'tumblr-authorize' );

	$tumblr = get_tumblr();
	$tumblr->getRequestHandler()->setBaseUrl( 'https://www.tumblr.com/' );

	$request = $tumblr->getRequestHandler()->request('POST', 'oauth/request_token', array(
		'oauth_callback' => admin_url('options-general.php?page=tumblr_crosspostr_settings&tumblr_crosspostr_callback')
	) );
	parse_str( $request->body->__toString(), $data );

	$tumblr->setToken( $data['oauth_token'], $data['oauth_token_secret'] );

	$url = sprintf( 'https://www.tumblr.com/oauth/authorize?oauth_token=%s', $data['oauth_token'] );
	wp_redirect( $url );

	die();
}

/**
*	OAuth connection workflow.
*	
*/
function authorize_check(){
	if( isset($_GET['tumblr_crosspostr_oauth_authorize']) ){
		add_action( 'init', __NAMESPACE__.'\authorize_app' );
	} else if( isset($_GET['tumblr_crosspostr_callback']) && !empty($_GET['oauth_verifier']) ){
		// Unless we're just saving the options, hook the final step in OAuth authorization.
		if( !isset($_GET['settings-updated']) ){
			add_action( 'init', __NAMESPACE__.'\authorize_complete' );
		}
	}
}
add_action( 'plugins_loaded', __NAMESPACE__.'\authorize_check' );

/**
*
*/
function authorize_complete(){
	$tumblr = get_tumblr();
	$tumblr->getRequestHandler()->setBaseUrl( 'https://www.tumblr.com/');

	$request = $tumblr->getRequestHandler()->request('POST', 'oauth/access_token', array(
		'oauth_verifier' => $_GET['oauth_verifier']
	) );
	
	parse_str( $request->body->__toString(), $data );

	// @TODO set an error message
	if( isset($data['oauth_token']) && isset( $data['oauth_token_secret']) ){
		update_option( 'tumblr_crosspostr_access_token', $data['oauth_token'] );
		update_option( 'tumblr_crosspostr_access_token_secret', $data['oauth_token_secret'] );	
	}
}

/**
*
*/
function admin_enqueue_scripts(){
	global $pagenow;
	
	switch( $pagenow ){
		case 'tools.php':
			wp_register_script( 'tumblr-crosspostr-tools', plugins_url('public/admin/tools.js', __FILE__), array('jquery') );
			wp_enqueue_script( 'tumblr-crosspostr-tools' );
			break;
			
		case 'post.php':
		case 'post-new.php':
			wp_register_script( 'tumblr-crosspostr-post', plugins_url('public/admin/post.js', __FILE__), array('jquery') );
			wp_enqueue_script( 'tumblr-crosspostr-post' );
			
			wp_register_style( 'tumblr-crosspostr-post', plugins_url('public/admin/post.css', __FILE__) );
			wp_enqueue_style( 'tumblr-crosspostr-post' );
			break;
	}
	
}
add_action( 'admin_enqueue_scripts', __NAMESPACE__.'\admin_enqueue_scripts' );

/**
*
*/
function admin_head(){
	register_contextual_help();
}
add_action( 'admin_head', __NAMESPACE__.'\admin_head' );

/**
*	delete from tumblr, from the trash list view
*	
*/
function delete_tumblr(){
	if( !isset($_GET['do']) || !isset($_GET['sjrnonce']) || !isset($_GET['post_id']) )
		return;
		
	$do = $_GET['do'];
	$sjrnonce = $_GET['sjrnonce'];
	$post_id = $_GET['post_id'];
	
	if( $do != 'delete_tumblr' || !wp_verify_nonce($sjrnonce, 'delete_tumblr-'.$post_id) )
		return;
		
	$tumblr = get_tumblr();
	
	$blog = get_tumblr_basename( $post_id );
	$tumblr_id = get_tumblr_id( $post_id );
	
	$response = $tumblr->deleteFromTumblrBlog( $blog, array(
		'id' => $tumblr_id
	) );
	
	if( $response->meta->status == 200 )
		echo '<p>Success</p>';
	else
		echo '';
}
add_action( 'load-edit.php', __NAMESPACE__.'\delete_tumblr' );

/**
*	
*	@param array
*	@return array
*/
function post_updated_messages( $messages ){
	if( isset($_GET['message_tumblr']) ){
		$message_tumblr = isset( $_GET['message_tumblr'] ) ? intval( $_GET['message_tumblr'] ) : '';
		$message_wp = isset( $_GET['message'] ) ? intval( $_GET['message'] ) : '';
		$post_id = intval( $_GET['post'] );

		switch( $message_tumblr ){
			case 1:
				$url = get_tumblr_link( $post_id );	
				$message = 'Post crossposted. <a href="' . $url . '">View post on Tumblr</a>';
				break;

			case 3:
				$message = '<p>Crossposting to Tumblr failed.</p>';
				$message .= get_transient( 'tumblr-crosspost-error' );
				delete_transient( 'tumblr-crosspost-error' );

				break;

			default:
				$message = 'Unknown Response from Tumblr';
				break;
		}

		foreach( $messages as &$post_type ){
			if( isset($post_type[$message_wp]) )
				$post_type[$message_wp] .= '<br/>'.$message;
		}
	}

	return $messages;
}
add_filter( 'post_updated_messages', __NAMESPACE__.'\post_updated_messages' );

/**
*	show link to tumblr post in admin screen
*	add delete link to trash screen
*	@param array
*	@param WP_Post
*	@return array
*/
function post_row_actions( $actions, $post ){
	$url = get_tumblr_link( $post->ID );
	
	if( $url )
		$actions['tumblr'] = '<a href="'.esc_attr( $url ).'">View on Tumblr</a>';
	
	if( $url && isset($actions['delete']) )
		$actions['delete_tumblr'] = '<a href="'.admin_url( 'edit.php?post_status=trash&post_type=post&do=delete_tumblr&sjrnonce='.wp_create_nonce( 'delete_tumblr-'.$post->ID ).'&post_id='.$post->ID ).'">Delete From Tumblr</a>';
		
	return $actions;
}
add_filter( 'page_row_actions', __NAMESPACE__.'\post_row_actions', 10, 2 );
add_filter( 'post_row_actions', __NAMESPACE__.'\post_row_actions', 10, 2 );

/**
*
*/
function register_admin_menu(){
	add_options_page(
		__('Tumblr Crosspostr Settings', 'tumblr-crosspostr'),
		__('Tumblr Crosspostr', 'tumblr-crosspostr'),
		'manage_options',
		'tumblr_crosspostr_settings',
		__NAMESPACE__.'\render_options_page'
	);

	add_management_page(
		__('Tumblrize Archives', 'tumblr-crosspostr'),
		__('Tumblrize Archives', 'tumblr-crosspostr'),
		'manage_options',
		'tumblr_crosspostr_crosspost_archives',
		__NAMESPACE__.'\render_tools_page'
	);
}
add_action( 'admin_menu', __NAMESPACE__.'\register_admin_menu' );
	
/**
*
*/
function register_contextual_help(){
	$screen = get_current_screen();
	if( $screen->id !== 'post' )
		return;
	
	$html = render( 'admin/post-context' );	
	
	$screen->add_help_tab( array(
		'id' => 'tumblr_crosspostr-' . $screen->base . '-help',
		'title' => __('Crossposting to Tumblr', 'tumblr-crosspostr'),
		'content' => $html
	) );

	$x = esc_html__( 'Tumblr Crosspostr:', 'tumblr-crosspostr' );
	$y = esc_html__( 'Tumblr Crosspostr support forum', 'tumblr-crosspostr' );
	$z = esc_html__( 'Donate to Tumblr Crosspostr', 'tumblr-crosspostr' );
	
	$sidebar = <<<END_HTML
<p><strong>$x</strong></p>
<p><a href="https://wordpress.org/support/plugin/tumblr-crosspostr" target="_blank">$y</a></p>
<p><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=meitarm%40gmail%2ecom&lc=US&item_name=Tumblr%20Crosspostr%20WordPress%20Plugin&item_number=tumblr%2dcrosspostr&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted" target="_blank">&hearts; $z &hearts;</a></p>
END_HTML;

	$screen->set_help_sidebar( $screen->get_help_sidebar() . $sidebar );
}

/**
*
*	@param object
*/
function render_meta_box( $post ){
	$tumblr = get_tumblr();
	if( !is_connected() ){
		show_error( 'Tumblr Crossposter does not yet have a connection to Tumblr. Are you sure you connected Tumblr Crosspostr to your Tumblr account?' );
		return;
	}
	
	$options = get_settings();

	// Set default crossposting options for this post.
	$base_hostname = get_tumblr_basename( $post->ID );
	
	//
	$tumblr_posts = get_tumblr_links( $post->ID );

	// 
	$api_response = $tumblr_posts ? get_tumlbr_live( $base_hostname, $tumblr_posts[0]->tumblr_id ) : FALSE;

	$vars = array(
		//'blogs' => blogs_select_field( array('name' => 'tumblr_crosspostr_destination'), $basename ),
		'api_response' => $api_response,
		'post_status' => $post->post_status,
		'send_to_tumblr' => get_post_meta( $post->ID, 'tumblr_crosspostr_crosspost', TRUE ),
		'source_url' => get_post_meta( $post->ID, 'tumblr_source_url', TRUE ),
		'tumblr_posts' => $tumblr_posts,
		'use_excerpt' => get_tumblr_use_excerpt( $post->ID )
	);
	
	echo render( 'admin/post-meta-box', $vars );
}
	
/**
*	Writes the HTML for the options page, and each setting, as needed.
*/
function render_options_page(){
	if( !current_user_can('manage_options') ){
		wp_die(__('You do not have sufficient permissions to access this page.', 'tumblr-crosspostr') );
	}
	
	$options = get_settings();
	
	$vars = (object) array(
		'additional_tags' => $options['additional_tags'],
		'auto_source' => $options['auto_source'],
		'connected' => TRUE,
		'consumer_key' => $options['consumer_key'],
		'consumer_secret' => $options['consumer_secret'],
		'debug' => $options['debug'],
		'default_hostname' => $options['default_hostname'],
		'exclude_categories' => $options['exclude_categories'],
		'exclude_tags' => $options['exclude_tags'],
		'redirect_on_page' => $options['redirect_on_page'],
		'redirect_permalinks' => $options['redirect_permalinks'],
		'selected_blogs' => blogs_select_field( array('id' => 'tumblr_crosspostr_default_hostname', 
													  'name' => 'tumblr_crosspostr_settings[default_hostname]'), 
													  get_tumblr_basename(0) ),
		'sync_posts' => blogs_list_checkboxes( array('id' => 'tumblr_crosspostr_sync_content', 
													 'name' => 'tumblr_crosspostr_settings[sync_content][]'), 
													 $options['sync_content'] ),
		'tumblr_reg_url' => get_tumblr_app_registration_url(),
		'use_excerpt' => $options['use_excerpt']
	);
	
	if( isset($_GET['disconnect']) && wp_verify_nonce($_GET['tumblr_crosspostr_nonce'], 'disconnect_from_tumblr') ){
		//get_tumblr()->client->ResetAccessToken();
		
		delete_option( 'tumblr_crosspostr_access_token' );
		delete_option( 'tumblr_crosspostr_access_token_secret' );
		
		$vars->connected = FALSE;
	}
	
	echo render( 'admin/options-general', $vars );
}
	
/**
*
*/
function render_tools_page(){
	$options = get_settings();
	
	$vars = array(
		'date_select' => render('admin/tools-date-select', array(
			'dates' => get_posts_year_month(),
		)),
		'default_hostname' => $options['default_hostname'],
		'post_types' => get_post_types( array(), 'objects' )
	);
	
	echo render( 'admin/tools', $vars );
}

/*
*	process form from /wp-admin/tools.php?page=tumblr_crosspostr_crosspost_archives
*/
function save_tools(){
	//
	global $wpdb;
	
	$post_types = isset( $_GET['post_types'] ) ? array_values( $_GET['post_types'] ) : array();
	$post_types = count( $post_types ) ? implode( ', ', array_map(function($pt){
		return "'".esc_sql( $pt )."'";
	}, $post_types) ) : "''";

	if( isset($_GET['export_by_month']) && isset($_GET['export_months']) && wp_verify_nonce($_GET['export_by_month'], 'export_by_month') ){
		$in_dates = array_map( function($date){
			return "'".esc_sql( $date )."'";
		}, $_GET['export_months'] );
		
		$in_dates = count($in_dates) ? implode( ', ', $in_dates ) : "''";
		
		$sql = "SELECT * FROM $wpdb->posts 
				WHERE post_status = 'publish'
				AND post_type IN( $post_types )
				AND CONCAT(YEAR(post_date), '-', MONTH(post_date)) IN( $in_dates )
				ORDER BY post_date ASC";
		$posts = $wpdb->get_results( $sql );
	} elseif( isset($_GET['export_by_ids']) && wp_verify_nonce($_GET['export_by_ids'], 'export_by_ids') ){
		$post_ids = $_GET['export_ids'];
		$post_ids = explode( ',', $post_ids );
		$post_ids = array_map( 'intval', $post_ids );
		$post_ids = count( $post_ids ) ? implode( ', ', $post_ids ) : "''";
		
		$sql = "SELECT * FROM $wpdb->posts 
				WHERE ID IN( $post_ids )
				ORDER BY post_date ASC";
		$posts = $wpdb->get_results( $sql );

	} elseif( isset($_GET['export_all']) && wp_verify_nonce($_GET['export_all'], 'export_all') ){
	
	} else {
		return;
	}

	//
	if( !is_connected() ){
		wp_redirect( admin_url('options-general.php?page=tumblr_crosspostr_settings') );
		exit();
	}
	
	$success = array();
	$errors = array();

	foreach( $posts as $post ){
		if( $prepared_post = prepare_for_tumblr($post->ID) ){
			
			$data = tumblr_api_post( $prepared_post );

			if( isset($data->id) ){
				
				$success[] = array( 
					'id' => $data->id, 
					'base_hostname' => $prepared_post->base_hostname
				);
			} else {
				$errors[] = array(
					'post_id' => $post->ID,
					'error_message' => $data->error
				);
			}

			// sleep  to prevent 'network saturation'
			sleep( 1 );
		}
	}
	
	$blogs = array();
	foreach( $success as $p ){
		$blogs[] = $p['base_hostname'];
	}
	
	if( count($success) ){
		echo render( 'admin/export-success', array(
			'blogs' => $blogs,
			'blogs_touched' => count( array_unique($blogs) ),
			'posts_touched' => count( $success ),
		) );
	}
	
	if( count($errors) ){
		echo render( 'admin/export-error', array(
			'errors' => $errors
		) );	
	}
}
add_action( 'load-tools_page_tumblr_crosspostr_crosspost_archives', __NAMESPACE__.'\save_tools' );

/**
*	register the settings for /wp-admin/options-general.php?page=tumblr_crosspostr_settings
*/
function settings_register(){
	register_setting(
		'tumblr_crosspostr_settings',
		'tumblr_crosspostr_settings',
		__NAMESPACE__.'\settings_validate'
	);
}
add_action( 'admin_init', __NAMESPACE__.'\settings_register' );

/**
*	validation callback for setting registered with register_setting()
*	@param array $input An array of of our unsanitized options.
*	@return array An array of sanitized options.
*/
function settings_validate( $input ){
	$safe_input = array();
	foreach( $input as $k => $v ){
		switch( $k ){
			case 'consumer_key':
				if( empty($v) ){
					$errmsg = __('Consumer key cannot be empty.', 'tumblr-crosspostr' );
					add_settings_error( 'tumblr_crosspostr_settings', 'empty-consumer-key', $errmsg);
				}
				$safe_input[$k] = sanitize_text_field($v);
			break;
			case 'consumer_secret':
				if( empty($v) ){
					$errmsg = __('Consumer secret cannot be empty.', 'tumblr-crosspostr' );
					add_settings_error( 'tumblr_crosspostr_settings', 'empty-consumer-secret', $errmsg);
				}
				$safe_input[$k] = sanitize_text_field($v);
			break;
			case 'default_hostname':
				$safe_input[$k] = sanitize_text_field($v);
			break;
			case 'sync_content':
				$safe_input[$k] = array();
				foreach( $v as $x){
					$safe_input[$k][] = sanitize_text_field($x);
				}
			break;
			case 'exclude_categories':
				$safe_v = array();
				foreach( $v as $x){
					$safe_v[] = sanitize_text_field($x);
				}
				$safe_input[$k] = $safe_v;
			break;
			case 'auto_source':
				if( 'Y' === $v || 'N' === $v){
					$safe_input[$k] = $v;
				}
			break;
			case 'debug':
			case 'exclude_tags':
			case 'redirect_on_page':
			case 'redirect_permalinks':
			case 'use_excerpt':
				$safe_input[$k] = intval($v);
			break;
			case 'additional_tags':
				if( is_string($v) ){
					$tags = explode(',', $v);
					$safe_tags = array();
					foreach( $tags as $t){
						$safe_tags[] = sanitize_text_field($t);
					}
					$safe_input[$k] = $safe_tags;
				}
			break;
		}
	}
	
	return $safe_input;
}
	
/**
*
*	shows missing configuration notice
*/
function show_config_notice(){
	$screen = get_current_screen();
	
	if( $screen->base === 'plugins' )
		echo render( 'admin/config-notice' );
}
	
/**
*	ajax action for selecting post types by month
*/
function tumblrize_post_types(){
	$post_types = isset( $_GET['post_types'] ) ? array_values( $_GET['post_types'] ) : NULL;
	
	echo render( 'admin/tools-date-select', array(
		'dates' => get_posts_year_month( $post_types ),
	) );
	
	die();
}
add_action( 'wp_ajax_tumblrize_post_types', __NAMESPACE__.'\tumblrize_post_types' );