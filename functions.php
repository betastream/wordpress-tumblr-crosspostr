<?php

namespace sjr\tumblr_poster;

/*
*
*	@param
*	@param bool
*	@return string
*/
function blogs_list_checkboxes( $attributes = array(), $selected = FALSE ){
	$html = '';
	$user_blogs = get_tumlbr_blogs();
	
	foreach( $user_blogs as $blog ){
		$html .= '<li>';
		$html .= '<label>';
		$x = parse_url( $blog->url, PHP_URL_HOST );
		$html .= '<input type="checkbox"';
		if( !empty($attributes) ){
			foreach( $attributes as $k => $v){
				$html .= ' ';
				switch( $k ){
					case 'id':
						$html .= $k . '="' . esc_attr($v) . '-' . esc_attr($x) . '"';
						break;
					default:
						$html .= $k . '="' . esc_attr($v) . '"';
						break;
				}
			}
		}
		
		if( $selected && in_array($x, $selected) ){
			$html .= ' checked="checked"';
		}
		
		$html .= ' value="' . esc_attr($x) . '"';
		$html .= '>';
		$html .= esc_html($blog->title) . '</label>';
		$html .= '</li>';
	}
	return $html;
}
	
/**
*	build <select> element for selecting default blog in settings
*	@param array
*	@param string
*	@return string
*/
function blogs_select_field( $attributes = array(), $selected = FALSE ){
	$html = '<select';
	if( !empty($attributes) ){
		foreach( $attributes as $k => $v ){
			$html .=  ' ' . $k . '="' . esc_attr($v) . '"';
		}
	}
	$html .= '>';
	
	$user_blogs = get_tumlbr_blogs();
	$enabled = array_map( function($blog){
		return $blog->url;
	}, $user_blogs );
	$enabled = apply_filters( 'sjr\tumblr_poster\enabled_blogs', $enabled );

	foreach( $user_blogs as $blog ){
		$host = parse_url( $blog->url, PHP_URL_HOST );
		$html .= '<option value="'. esc_attr( $host ) .'" '. 
					selected( $selected, $host, FALSE ) .' '.
					disabled( !in_array($blog->url, $enabled), TRUE, FALSE ).'>'.
					esc_html( $blog->title ).' - '.$host.
				 '</option>';
	}
	$html .= '</select>';
	return $html;
}
	
// TODO: Add error handling for when the $post_body doesn't give
//   	us what we need to fulfill the Tumblr post type req's.
/*
*	Extracts a given string from another string according to a regular expression.
*
*	@param string $pattern The PCRE-compatible regular expression.
*	@param string $str The source from which to extract text matching the $pattern.
*	@param int $group If the regex uses capture groups, the number of the capture group to return.
*	@return string The matched text.
*/
function extract_by_regex( $pattern, $str, $group = 0 ){
	$matches = array();
	preg_match( $pattern, $str, $matches );
	
	return isset( $matches[$group] ) ? $matches[$group] : '';
}

/*
*
*	@return
*/
function get_tumblr_app_registration_url(){
	/*
	$params = array(
		'title' => get_bloginfo('name'),
		// Max 400 chars for Tumblr
		'description' => mb_substr(get_bloginfo('description'), 0, 400, get_bloginfo('charset')),
		'url' => home_url(),
		'admin_contact_email' => get_bloginfo('admin_email'),
		'default_callback_url' => plugins_url('/oauth-callback.php', __FILE__)
	);
	
	$tumblr = get_tumblr();
	return $tumblr->getAppRegistrationUrl( $params );
	*/
}

/*
*
*	@param array
*	@return array
*/
function get_posts_year_month( $post_types = array() ){
	global $wpdb;
	$post_types = count( $post_types ) ? implode( ', ', array_map(function($pt){
		return "'".esc_sql( $pt )."'";
	}, $post_types) ) : "''";
	
	$sql = "SELECT ID, post_title, COUNT(ID) as `total`, YEAR(post_date) as `year`, MONTH(post_date) AS `month`
			FROM $wpdb->posts 
			WHERE post_status = 'publish'
			AND post_type IN( $post_types )
			GROUP BY year, month
			ORDER BY post_date DESC";
	$res = $wpdb->get_results( $sql );
	return $res;
}

/*
*
*	@return array
*/
function get_settings(){
	$settings = (array) get_option( 'tumblr_crosspostr_settings' );
	
	$defaults = array(
		'additional_tags' => '',
		'auto_source' => '',
		'consumer_key' => '',
		'consumer_secret' => '',
		'debug' => 0,
		'default_hostname' => '',
		'exclude_categories' => array(),
		'exclude_tags' => '',
		'last_synced_ids' => array(),
		'redirect_on_page' => 0,
		'redirect_permalinks' => 0,
		'sync_content' => FALSE,
		'use_excerpt' => 0
	);
	
	$settings = array_merge( $defaults, $settings );
	
	return $settings;
}

/*
*
*	@return Tumblr_Crosspostr_API_Client
*/
/*
function get_tumblr(){
	static $tumblr = NULL;
	
	if( is_null($tumblr) ){
		$options = get_settings();

		if( $options['consumer_key'] && $options['consumer_secret'] ){
			$tumblr = new \Tumblr_Crosspostr_API_Client( $options['consumer_key'], $options['consumer_secret'] );
		} else {
			$tumblr = new \Tumblr_Crosspostr_API_Client;
		}
		
		if( get_option('tumblr_crosspostr_access_token') && get_option('tumblr_crosspostr_access_token_secret') ){
			$tumblr->client->access_token = get_option('tumblr_crosspostr_access_token' );
			$tumblr->client->access_token_secret = get_option( 'tumblr_crosspostr_access_token_secret' );
		}
			
		if( $options['debug'] ){
			$tumblr->client->debug = 1;
			$tumblr->client->debug_http = 1;
		}
	}

	return $tumblr;
}
*/

/*
*
*	@return Tumblr\API\Client
*/
function get_tumblr(){
	static $tumblr = NULL;

	if( is_null($tumblr) ){
		$options = get_settings();

		if( $options['consumer_key'] && $options['consumer_secret'] ){
			$tumblr = new \Tumblr\API\Client( $options['consumer_key'], $options['consumer_secret'] );
		} else {
			$tumblr = new \Tumblr\API\Client( '', '' );
		}

		if( ($token = get_option('tumblr_crosspostr_access_token')) && ($token_secret = get_option('tumblr_crosspostr_access_token_secret')) ){
			$tumblr->setToken( $token, $token_secret );
		}
	}

	return $tumblr;
}

/*
*
*	@param int
*	@return string
*/
function get_tumblr_basename( $post_id ){
	$d = get_post_meta( $post_id, '_tumblr_base_hostname', TRUE );

	if( empty($d) ){
		$options = get_settings();
		$d = $options['default_hostname'];
	}
	
	return $d;
}

/*
*
*	@return array
*/
function get_tumlbr_blogs(){
	$tumblr = get_tumblr();

	try{
		$user_info = $tumblr->getUserInfo();
		$blogs = $user_info->user->blogs;
	} catch( \Tumblr\API\RequestException $e ){
		 $blogs = array();
	}
	
	return $blogs;
}

/*
*
*	@param int
*	@return
*/
function get_tumblr_cache( $post_id ){
	$time = current_time( 'timestamp' );

	$last_updated = (int) get_post_meta( $post_id, '_tumblr-api-lastcall', TRUE );
	$post = get_post_meta( $post_id, '_tumblr-api-response', TRUE );

	if( ($last_updated < $time - 3600) || empty($post) )
		$post = update_tumblr_cache( $post_id );

	return $post;
}

/*
*
*	@param string
*	@param int
*	@return object
*/
function get_tumlbr_live( $blog, $tumblr_id ){
	$tumblr = get_tumblr();

	try{
		$data = $tumblr->getBlogPosts( $blog, array('id' => $tumblr_id) );
		$post = isset($data->posts) ? $data->posts[0] : FALSE;

	} catch( \Tumblr\API\RequestException $e ){
		$post = (object) array(
			'error' => $e->getMessage()
		);
	}

	return $post;
}

/*
*
*	@param int
*	@return int
*/
function get_tumblr_id( $post_id ){
	return get_post_meta( $post_id, '_tumblr_post_id', TRUE );
}

/**
*
*	@param int
*	@return array
*/
function get_tumblr_ids( $post_id ){
	$ids = get_post_meta( $post_id, '_tumblr_post_id', FALSE );

	return $ids;
}

/*
*
*	@param int
*	@return string
*/
function get_tumblr_link( $post_id ){
	$tumblr_basename = get_tumblr_basename( $post_id );
	$tumblr_post_id = get_tumblr_id( $post_id );
	
	if( !$tumblr_post_id )
		return FALSE;

	$tumblr_data = get_tumblr_cache( $post_id );
	
	$return = 'http://' . $tumblr_basename . '/post/' . $tumblr_post_id;
	if( !empty($tumblr_data->slug) )
		$return .= '/'.$tumblr_data->slug;

	return $return;
}

/**
*
*	@param int
*	@return array
*/
function get_tumblr_links( $post_id ){
	$return = array();
	
	$tumblr_basename = get_tumblr_basename( $post_id );
	$tumblr_post_ids = get_tumblr_ids( $post_id );

	foreach( $tumblr_post_ids as $tumblr_post_id ){
		$return[] = (object) array(
			'basename' => $tumblr_basename,
			'tumblr_id' => $tumblr_post_id,
			'url' => 'http://' . $tumblr_basename . '/post/' . $tumblr_post_id,
			'wp_post_id' => $post_id
		);
	}
	
	return $return;
}

/*
*
*	@param int
*	@return int
*/
function get_tumblr_use_excerpt( $post_id ){
	$e = get_post_meta( $post_id, 'tumblr_crosspostr_use_excerpt', TRUE );
	if( empty($e) ){
		$options = get_settings();
		$e = $options['use_excerpt'];
	}
	
	return intval( $e );
}

/*
*
*	@return bool
*/
function is_connected(){
	return !!get_option( 'tumblr_crosspostr_access_token' );
}
	
/*
*
*	@param int
*	@return
*/
function is_post_crosspostable( $post_id ){
	$options = get_settings();
	$crosspostable = TRUE;

	// Do not crosspost if this post is excluded by a certain category.
	if( in_category($options['exclude_categories'], $post_id) ){
		$crosspostable = FALSE;
	}

	// Do not crosspost if this specific post was excluded.
	if( 'Y' !== get_post_meta($post_id, 'tumblr_crosspostr_crosspost', TRUE) ){
		$crosspostable = FALSE;
	}

	// Do not crosspost unsupported post states.
	if( !wp_status_to_tumblr_state(get_post_status($post_id)) ){
		$crosspostable = FALSE;
	}

	return $crosspostable;
}

/*
*
*	@param string tumblr url or tumblr post id
*	@return int
*/
function parse_tumblr_id( $url ){
	if( is_int($url) || is_numeric($url) ){
		$id = intval( $url );
	} elseif( 1 ){
		preg_match( '/post\/(\w+)/i', $url, $ids );
		$id = isset($ids[1]) ? intval( $ids[1] ) : intval( $url );
	} else {
		$id = intval( $url );
	}
	
	return $id;
}

/*
*	show the link to tumblr post instead of wordpress link
*	@param string
*	@param WP_Post
*	@param bool
*	@param bool
*	@return string
*/
function post_link( $post_link, $post, $leavename, $sample = FALSE ){
	if( !is_admin() && ($url = get_tumblr_link($post->ID)) )
		return $url;
	
	return $post_link;
}

/*
*	Translates a WordPress post data for Tumblr's API.
*	performs actions
*		sjr\tumblr_poster\pre_prepare_post
*		sjr\tumblr_poster\post_prepare_post
*	performs filters
*		sjr\tumblr_poster\prepared_post
*	@param int $post_id The ID number of the WordPress post.
*	@return mixed A simple object representing data for Tumblr or FALSE if the given post should not be crossposted.
*/
function prepare_for_tumblr( $post_id ){
	if( !is_post_crosspostable($post_id) )
		return FALSE;
	
	do_action( 'sjr\tumblr_poster\pre_prepare_post', $post_id );
	
	$options = get_settings();
	$custom = get_post_custom( $post_id );

	$prepared_post = (object) array(
		'post_id' => $post_id
	);

	// Set the post's Tumblr destination.
	$base_hostname = FALSE;
	
	$base_hostname = sanitize_text_field( $options['default_hostname'] );
	$prepared_post->base_hostname = $base_hostname;
	update_post_meta( $post_id, '_tumblr_base_hostname', $base_hostname );
	
	// Set "Content source" meta field.
	if( !empty($_POST['tumblr_crosspostr_meta_source_url']) ){
		$source_url = sanitize_text_field($_POST['tumblr_crosspostr_meta_source_url'] );
		update_post_meta( $post_id, 'tumblr_source_url', $source_url );
	} else if( !empty($custom['tumblr_source_url'][0]) ){
		$source_url = sanitize_text_field( $custom['tumblr_source_url'][0] );
	} else if( 'Y' === $options['auto_source'] ){
		$source_url = get_permalink( $post_id );
		delete_post_meta( $post_id, 'tumblr_source_url' );
	} else {
		$source_url = FALSE;
	}

	$format = get_post_format( $post_id );
	$state = wp_status_to_tumblr_state( get_post_status($post_id) );
	
	$tags = array();
	if( $t = get_the_tags($post_id) ){
		foreach( $t as $tag ){
			// Decode manually so that's the ONLY decoded entity.
			$tags[] = str_replace('&amp;', '&', $tag->name);
		}
	}
	
	$common_params = array(
		'type' => wp_post_format_to_tumblr_post_type($format),
		'state' => $state,
		'tags' => implode(',', $tags),
		'date' => get_post_time('Y-m-d H:i:s', TRUE, $post_id) . ' GMT',
		'format' => 'html', // Tumblr's "formats" are always either 'html' or 'markdown'
		'slug' => get_post_field('post_name', $post_id),
	);
	
	if( $source_url)
		$common_params['source_url'] = $source_url; 

	if( !empty($options['exclude_tags']) )
		unset( $common_params['tags'] );

	if( !empty($options['additional_tags']) ){
		if( !isset($common_params['tags']) ){
			$common_params['tags'] = '';
		}
		$common_params['tags'] = implode(',', array_merge(explode(',', $common_params['tags']), $options['additional_tags']) );
	}
	
	if( $common_params['state'] == 'queue' )
		$common_params['publish_on'] = get_post_time( 'Y-m-d H:i:s', TRUE, $post_id );

	$post_params = prepare_params_by_post_type( $post_id, $common_params['type'] );

	//
	if( isset($_POST['tumblr_crosspostr_send_tweet']) ){
		if( !empty($_POST['tumblr_crosspostr_tweet_text']) ){
			$prepared_post->params['tweet'] = sanitize_text_field( $_POST['tumblr_crosspostr_tweet_text'] );
		}
	}

	$prepared_post->params = array_merge( $common_params, $post_params );

	if( $tumblr_id = get_tumblr_id($post_id) )
		$prepared_post->params['id'] = $tumblr_id;
	
	$prepared_post = apply_filters( 'sjr\tumblr_poster\prepared_post', $prepared_post, $post_id );

	do_action( 'sjr\tumblr_poster\post_prepare_post', $post_id, $prepared_post );

	return $prepared_post;
}
	
/*
*
*	@param int
*	@param string
*	@return array
*/
function prepare_params_by_post_type( $post_id, $type ){
	$post_body = get_post_field( 'post_content', $post_id );
	$post_excerpt = get_post_field( 'post_excerpt', $post_id );
	
	// Mimic wp_trim_excerpt() without The Loop.
	if( empty($post_excerpt) ){
		$text = $post_body;
		$text = strip_shortcodes($text);
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]&gt;', $text);
		$text = wp_trim_words($text);
		$post_excerpt = $text;
	}

	$use_excerpt = get_tumblr_use_excerpt( $post_id ); // Use excerpt?
	$r = array();
	
	switch( $type ){
		case 'photo':
			$r['caption'] = $use_excerpt
				? apply_filters('the_excerpt', $post_excerpt)
				: apply_filters('the_content', strip_only($post_body, 'img', TRUE, 1) );

			$r['link'] = ''; /* extract_by_regex('/<img.*?src="(.*?)".*?\/?>/', $post_body, 1); */
			$r['source'] = ''; /* extract_by_regex('/<img.*?src="(.*?)".*?\/?>/', $post_body, 1); */
			break;
		case 'quote':
			$pattern = '/<blockquote.*?>(.*?)<\/blockquote>/s';
			$r['quote'] = wpautop(extract_by_regex($pattern, $post_body, 1) );
			$len = strlen(extract_by_regex($pattern, $post_body, 0) );

			$r['source'] = $use_excerpt
				? apply_filters('the_excerpt', $post_excerpt)
				: apply_filters('the_content', substr($post_body, $len) );
			break;
		case 'link':
			$r['title'] = tubmlr_html_title( get_post_field('post_title', $post_id) );
			$r['url'] = ($use_excerpt && preg_match('/<a.*?href="(.*?)".*?>/', $post_excerpt))
				? extract_by_regex('/<a.*?href="(.*?)".*?>/', $post_excerpt, 1)
				: extract_by_regex('/<a.*?href="(.*?)".*?>/', $post_body, 1);
			$r['description'] = $use_excerpt
				? apply_filters('the_excerpt', $post_excerpt)
				: apply_filters('the_content', $post_body);
			break;
		case 'chat':
			$r['title'] = tubmlr_html_title( get_post_field('post_title', $post_id) );
			$r['conversation'] = wp_strip_all_tags($post_body);
			break;
		case 'audio':
			$r['caption'] = $use_excerpt
				? apply_filters('the_excerpt', $post_excerpt)
				: apply_filters('the_content', $post_body);
			// Strip after apply_filters in case shortcode is used to generate <audio> element.
			$r['caption'] = strip_only($r['caption'], 'audio', 1);
			$r['external_url'] = extract_by_regex('/(?:href|src)="(.*?\.(?:mp3|wav|wma|aiff|ogg|ra|ram|rm|mid|alac|flac))".*?>/i', $post_body, 1);
			break;
		case 'video':
			$r['caption'] = $use_excerpt
				? apply_filters('the_excerpt', $post_excerpt)
				: apply_filters('the_content', strip_only($post_body, 'iframe', TRUE, 1) );

			$pattern_youtube = '/youtube(?:-nocookie)\.com\/(?:v|embed)\/([\w\-]+)/';
			$pattern_vimeo = '/player\.vimeo\.com\/video\/([0-9]+)/';
				
			if( preg_match($pattern_youtube, $post_body) ){
				$r['embed'] = 'https://www.youtube.com/watch?v='
					. extract_by_regex($pattern_youtube, $post_body, 1);
			} else if( preg_match($pattern_vimeo, $post_body) ){
				$r['embed'] = '<iframe src="//' . extract_by_regex($pattern_vimeo, $post_body, 0) . '">'
					. '<a href="//vimeo.com/' . extract_by_regex($pattern_vimeo, $post_body, 1). '">'
					. esc_html__('Watch this video.', 'tumblr-crosspostr') . '</a></iframe>';
			} else {
				// Pass along the entirety of any unrecognized <iframe>.
				$r['embed'] = extract_by_regex('/<iframe.*?<\/iframe>/', $post_body, 0);
			}
			break;
		case 'text':
			$r['title'] = tubmlr_html_title( get_post_field('post_title', $post_id) );
			// fall through
		case 'aside':
		default:
			$r['body'] = $use_excerpt
				? apply_filters('the_excerpt', $post_excerpt)
				: apply_filters('the_content', $post_body);
			break;
	}
	
	if( !current_user_can('unfiltered_html') ){
		$r['title'] = htmlspecialchars_decode( $r['title'] );
	}

	return $r;
}

/*
*	add query var to show tumblr generic error message
*	@param string
*	@param int
*	@return string
*/
function redirect_post_location_error( $location, $post_id ){
	$location .= '&message_tumblr=3';

	return $location;
}

/*
*	add query var to show tumblr success message
*	@param string
*	@param int
*	@return string
*/
function redirect_post_location_published( $location, $post_id ){
	$location .= '&message_tumblr=1';

	return $location;
}
	
/*
* render a page into wherever
* @param string filename inside /views/ directory, no trailing .php
* @param object|array variables available to view
* @return string html
*/
function render( $filename, $vars = array() ){
	extract( (array) $vars, EXTR_SKIP );
	
	ob_start();
	require __DIR__.'/views/'.$filename.'.php';
	$html = ob_get_contents();
	ob_end_clean();
	
	return $html;
}

/*
*
*	@param object
*	@return string
*/
function translate_tumblr_post_content( $post ){
	$content = '';
	switch( $post->type ){
		case 'photo':
			foreach( $post->photos as $photo ){
				$content .= '<img src="' . $photo->original_size->url . '" alt="" />';
				$content .= $post->caption;
			}
			break;
		case 'quote':
			$content .= '<blockquote>' . $post->text . '</blockquote>';
			$content .= $post->source;
			break;
		case 'link':
			$content .= '<a href="' . $post->url . '">' . $post->title . '</a>';
			$content .= $post->description;
			break;
		case 'audio':
			$content .= $post->player;
			$content .= $post->caption;
			break;
		case 'video':
			$content .= $post->player[0]->embed_code;
			$content .= $post->caption;
			break;
		case 'answer':
			$content .= '<a href="' . $post->asking_url .'" class="tumblr_blog">' . $post->asking_name . '</a>:';
			$content .= '<blockquote cite="' . $post->post_url . '" class="tumblr_ask">' . $post->question . '</blockquote>';
			$content .= $post->answer;
			break;
		case 'chat':
		case 'text':
		default:
			$content .= $post->body;
			break;
	}
	return $content;
}

/**
*	create or edit the post on tumblr
*	@param object
*	@return object
*/
function tumblr_api_post( $prepared_post ){
	$tumblr = get_tumblr();

	try{
		if( !empty($prepared_post->params['id']) ){
			$data = $tumblr->editPost( $prepared_post->base_hostname, $prepared_post->params['id'], $prepared_post->params );
		} else {
			$data = $tumblr->createPost( $prepared_post->base_hostname, $prepared_post->params );
		}

		// $data is { id: (int) }
		update_tumblr_cache( $prepared_post->post_id );
		update_post_meta( $prepared_post->post_id, '_tumblr_post_id', $data->id );

	} catch( \Tumblr\API\RequestException $e ){
		$data = (object) array(
			'error' => $e->getMessage()
		);
	}

	return $data;
}

/**
*	convert <br/> to new line and strip other tags
*	@param string
*	@return string
*/
function tubmlr_html_title( $title ){
	$title = preg_replace( '#<br\s*/?>#i', "\n", $title );
	return strip_tags( $title );
}

/**
*
*	@param string
*	@return string
*/
function tumblr_post_type_to_wp_post_format( $type ){
	switch( $type ){
		case 'quote':
		case 'audio':
		case 'video':
		case 'chat':
		case 'link':
			$format = $type;
			break;
		case 'photo':
			$format = 'image';
			break;
		case 'answer':
		case 'text':
		default:
			$format = '';
	}
	return $format;
}
	
/**
*	translates a Tumblr post state to a WordPress post status.
*	@param string
*	@return string
*/
function tumblr_state_to_wp_status( $state ){
	switch( $state ){
		case 'draft':
		case 'private':
			$status = $state;
			break;
		case 'queue':
			// $status = 'future';
			$status = 'private';
			break;
		case 'published':
		default:
			$status = 'publish';
	}

	return $status;
}

/**
*
*	@param string
*/
function show_error( $msg ){
	?>
	<div class="error">
		<p><?php print esc_html( $msg ); ?></p>
	</div>
	<?php
}

/**
*
*	@param string
*/
function show_notice( $msg ){
	?>
	<div class="updated">
		<p><?php print $msg; // No escaping because we want links, so be careful. ?></p>
	</div>
	<?php
}
	
/**
*	Modified from https://stackoverflow.com/a/4997018/2736587 which claims
*	http://www.php.net/manual/en/function.strip-tags.php#96483
*	as its source. Werksferme.
*	@param
*	@param
*	@param
*	@param
*	@return
*/
function strip_only( $str, $tags, $stripContent = FALSE, $limit = -1 ){
	$content = '';
	if(!is_array($tags) ){
		$tags = (strpos($str, '>') !== FALSE ? explode('>', str_replace('<', '', $tags)) : array($tags) );
		if( end($tags) == '' )
			array_pop( $tags );
	}
	
	foreach( $tags as $tag ){
		if( $stripContent ){
			$content = '(.+</'.$tag.'[^>]*>|)';
		}
		
		$str = preg_replace( '#</?'.$tag.'[^>]*>'.$content.'#is', '', $str, $limit );
	}
	
	return $str;
}

/**
*
*	@param int
*	@return
*/
function update_tumblr_cache( $post_id ){
	$blog = get_tumblr_basename( $post_id );
	$time = current_time( 'timestamp' );
	$tumblr_id = get_tumblr_id( $post_id );

	$post = get_tumlbr_live( $blog, $tumblr_id );

	update_post_meta( $post_id, '_tumblr-api-lastcall', $time );
	update_post_meta( $post_id, '_tumblr-api-response', $post );

	return $post;
}

/**
*
*	@param string
*	@return string
*/
function wp_post_format_to_tumblr_post_type( $format ){
	switch( $format ){
		case 'audio':
		case 'chat':
		case 'link':
		case 'quote':
		case 'video':
			$type = $format;
			break;

		case 'image':
		case 'gallery':
			$type = 'photo';
			break;
			
		case 'aside':
		case FALSE:
		default:
			$type = 'text';
			break;
	}
	return $type;
}	

/**
*	translates a WordPress post status to a Tumblr post state.
*	@param string $status The WordPress post status to translate.
*	@return mixed The translates Tumblr post state or FALSE if the WordPress status has no equivalently compatible state on Tumblr.
*/
function wp_status_to_tumblr_state( $status ){
	switch( $status ){
		case 'draft':
		case 'private':
			$state = $status;
			break;

		case 'future':
			// $state = 'queue';
			$state = 'private';
			break;

		case 'publish':
			$state = 'published';
			break;
		
		case 'auto-draft':
		case 'inherit':
		case 'pending':
		default:
			$state = FALSE;
	}
	
	return $state;
}