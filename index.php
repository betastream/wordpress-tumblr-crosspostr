<?php
/*
*	Plugin Name:	Tumblr Crosspostr
*	Plugin URI: 
*	Description:	Automatically crossposts to your Tumblr blog when you publish a post on your WordPress blog. Forked from https://github.com/meitar/tumblr-crosspostr/#readme by Meitar Moscovitz v0.7.14 http://Cyberbusking.org/
*	Version:		1.0.7
*	Author: 		Group SJR
*	Author URI: 
*	Text Domain:	tumblr-crosspostr
*	Domain Path:	/lang
*/
 
namespace sjr\tumblr_poster;

if( defined('WP_LOAD_IMPORTERS') || defined('DOING_CRON') )
	return;

require __DIR__.'/functions.php';
require __DIR__.'/post.php';

// @TODO should be autoloaded! confirm
require __DIR__.'/lib/class-plugin-oauthwp.php';
require __DIR__.'/lib/oauth_api/oauth_client.php';

require __DIR__.'/lib/class-tumblr-crosspostr.php';
require __DIR__.'/lib/class-oAuthWP.php';
require __DIR__.'/lib/class-oAuthWP-Tumblr.php';
require __DIR__.'/lib/class-tumblr-crosspostr-APIClient.php';
require __DIR__.'/lib/httpclient/http.php';

if( is_admin() )
	require __DIR__.'/admin.php';

/*
*
*	@param string
*	@return
*/
function autoload_register( $class ){
	if( !class_exists($class) ){
		$class = str_replace( '\\', '/', $class );
		$path = __DIR__.'/lib/'.$class.'.php';

		if( file_exists($path) )
			require_once $path;
	}
}
spl_autoload_register( __NAMESPACE__.'\autoload_register' );

/**
*	add filters for redirection
*/
function plugins_loaded(){
	$settings = get_settings();
	
	// replace wp permalinks with tumblr links
	if( $settings['redirect_permalinks'] ){
		add_filter( 'post_link', __NAMESPACE__.'\post_link', 10, 3 );
		add_filter( 'post_type_link', __NAMESPACE__.'\post_link', 10, 4 );
	}
	
	// redirect to tumblr page from wp page
	if( $settings['redirect_on_page'] ){
		add_filter( 'posts_results', __NAMESPACE__.'\posts_results', 10, 2 );
	}	
}
add_action( 'plugins_loaded', __NAMESPACE__.'\plugins_loaded' );

/**
*	redirect single pages to the tumblr post, if it exists
*	@param array
*	@param WP_Query
*	@return array
*/
function posts_results( $posts, $wp_query ){
	if( $wp_query->is_main_query() && $wp_query->is_singular() && count($posts) ){
		if( $url = get_tumblr_link($posts[0]->ID) ){
			wp_redirect( $url, 301 );
			die();
		}
	}
	
	return $posts;
}

/**
*
*/
function register_l10n(){
	load_plugin_textdomain( 'tumblr-crosspostr', false, dirname(plugin_basename(__FILE__)) . '/languages/' );
}
add_action( 'plugins_loaded', __NAMESPACE__.'\register_l10n' );

/**
*	run late, so themes have a chance to register support
*/
function register_theme_support(){
	$formats = array(
		'link',
		'image',
		'quote',
		'video',
		'audio',
		'chat'
	);
	$x = get_theme_support( 'post-formats' );
	$f = empty( $x ) ? array() : $x[0];
	$diff = array_diff( $formats, $f );
	
	add_theme_support( 'post-formats', array_merge($f, $diff) );
}
add_action( 'after_setup_theme', __NAMESPACE__.'\register_theme_support', 700 );

