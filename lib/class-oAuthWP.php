<?php
/**
 * OAuthWP is a base class useful for creating WordPress plugins that
 * use any version of the OAuth protocol. It has two primary and very
 * simple classes: OAuthWP and Plugin_OAuthWP.
 *
 * OAuthWP extends Manuel Lemos's oauth_client_class to support each
 * version of the OAuth protocol (1, 1.0a, and 2.0).
 *
 * Plugin_OAuthWP is an abstract class that provides a skeleton for
 * common methods needed by WordPress plugins that use OAuth.
 * (For example, its $client property is an instance of OAuthWP.)
 */

abstract class OAuthWP extends oauth_client_class {

	public function authorize ($redirect_uri){
		$this->redirect_uri = $redirect_uri;
		$s = $this->Process();
		
		if( $this->exit ){
			$this->Finalize( $s );
			exit();
		}
	}

	public function completeAuthorization ($redirect_uri){
		$this->redirect_uri = $redirect_uri;
		$this->Process();
		$tokens = array();
		$this->GetAccessToken($tokens );
		return $tokens;
	}
}
