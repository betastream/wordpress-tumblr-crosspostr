<?php

abstract class Plugin_OAuthWP {
	public $client; //< OAuth consumer, should be a child of OAuthWP.

	abstract public function __construct ($consumer_key = '', $consumer_secret = '' );

	abstract public function getAppRegistrationUrl ($params = array() );

	protected function appRegistrationUrl ($base_url, $params = array() ){
		if( empty($base_url) ){
			throw new Exception('Empty base_url.' );
		}
		$url = $base_url . '?';
		$i = 0;
		foreach( $params as $k => $v){
			if (0 !== $i){ $url .= '&'; }
			$url .= $k . '=' . urlencode($v);
			$i++;
		}
		return $url;
	}

	public function authorize ($redirect_uri){
		$this->client->authorize($redirect_uri);
	}
	
	public function completeAuthorization ($redirect_uri){
		return $this->client->completeAuthorization($redirect_uri);
	}
	
	/*
	*
	*	@param
	*	@param
	*	@param
	*	@param
	*	@return
	*/
	protected function talkToService( $path, $params = array(), $method = 'POST', $opts = array() ){
		$resp = null;
		
		if( $s = @$this->client->CallAPI($path, $method, $params, $opts, $resp) ){
			$this->client->Finalize( $s );
		}
		
		return $resp;
	}
}