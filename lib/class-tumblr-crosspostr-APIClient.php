<?php
/**
 * Super-skeletal class to interact with Tumblr from Tumblr Crosspostr plugin.
 */

class Tumblr_Crosspostr_API_Client extends Tumblr_OAuthWP_Plugin {
	private $api_key; //< Also the "Consumer key" the user entered.

	function __construct( $consumer_key = '', $consumer_secret = '' ){
		$this->client = new OAuthWP_Tumblr;
		$this->client->server = 'Tumblr';
		$this->client->client_id = $consumer_key;
		$this->client->client_secret = $consumer_secret;
		$this->client->configuration_file = __DIR__.'/oauth_api/oauth_configuration.json';
		$this->client->Initialize();

		if( $consumer_key )
			$this->set_api_key( $consumer_key );

		return $this;
	}

	// Needed for some GET requests.
	public function set_api_key( $key ){
		$this->api_key = $key;
	}
	
	/*
	*
	*	@param bool set to false to use cached data
	*	@return array
	*/
	public function getUserBlogs( $live = TRUE ){
		if( !$live ){
			$data = get_transient( 'sjr-tumblr-blogs' );
		}
		
		if( $live || $data === FALSE ){
			$data = $this->talkToService('/user/info', array(), 'GET' );
			set_transient( 'sjr-tumblr-blogs', $data, DAY_IN_SECONDS );
		}
		
		// TODO: This could use some error handling?
		if( !empty($data->response->user) ){
			return $data->response->user->blogs;
		} else {
			return array();
		}
	}
	
	/*
	*
	*	@param string
	*	@return
	*/
	public function getBlogInfo( $base_hostname ){
		$data = $this->talkToService("/blog/$base_hostname/info?api_key={$this->api_key}", array(), 'GET' );
		
		switch( $data->meta->status ){
			case 401:
			case 404:
				return $data->meta->msg;
				break;
				
			default:

				break;
		}
		
		return $data->response->blog;
	}
	
	/*
	*
	*	@param string
	*	@param array
	*	@return
	*/
	public function getPosts( $base_hostname, $params = array() ){
		$url = "/blog/$base_hostname/posts?api_key={$this->api_key}";

		if( !empty($params) ){
			foreach( $params as $k => $v){
				$url .= "&$k=$v";
			}
		}
		
		$data = $this->talkToService( $url, array(), 'GET' );
		
		return $data->response;
	}
	
	/*
	*
	*	@param
	*	@param
	*	@return
	*/
	public function postToTumblrBlog( $blog, $params ){
		$api_method = "/blog/$blog/post";
		$ok = $this->talkToService( $api_method, $params );
		
		return $ok;
	}
	
	/*
	*
	*	@param string
	*	@param
	*	@return
	*/
	public function editOnTumblrBlog( $blog, $params ){
		//require __DIR__.'/upload.php';
		//return new_post( $blog, $params );

		$api_method = "/blog/$blog/post/edit";
		return $this->talkToService( $api_method, $params );
	}
	
	/*
	*
	*	@param string
	*	@param
	*	@return
	*/
	public function deleteFromTumblrBlog( $blog, $params ){
		$api_method = "/blog/$blog/post/delete";
		return $this->talkToService($api_method, $params );
	}

}
