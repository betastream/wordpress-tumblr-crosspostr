<?php

namespace sjr\tumblr_poster;
	
class Tumblr_Crosspostr {
	private $tumblr; //< Tumblr API manipulation wrapper.
	
	/*
	*
	*/
	public function __construct(){
		add_action( 'before_delete_post', array($this, 'before_delete_post') );
		
		
		$this->tumblr = get_tumblr();
		
		$options = get_settings();
	
		if( !is_connected() ){
			add_action( 'admin_notices', __NAMESPACE__.'\show_config_notice' );
		}
		
		
	}

	/**
 	* Issues a Tumblr API call.
 	*
 	* @param string $blog The Tumblr blog's base hostname.
 	* @param array @params Any additional parameters for the request.
 	* @param bool $deleting Whether or not to delete, rather than to edit, a specific Tumblr post.
 	* @return array Tumblr's decoded JSON response.
 	*/
	private function crosspostToTumblr( $blog, $params, $deleting = false ){
		// @TODO Smoothen this deleting thing.
		// Cancel WordPress deletions if Tumblr deletions aren't working?
		if( $deleting === true && !empty($params['id']) ){
			return $this->tumblr->deleteFromTumblrBlog( $blog, $params );
		} else if( !empty($params['id']) ){
			$response = $this->tumblr->editOnTumblrBlog( $blog, $params );
			return $response;
		} else {
			return $this->tumblr->postToTumblrBlog( $blog, $params );
		}
	}
	
	/*
	*
	*	@param int wordpress post id
	*	@return
	*/
	public function before_delete_post( $post_id ){
		$tumblr_id = get_tumblr_id( $post_id );
		$this->crosspostToTumblr( get_tumblr_basename($post_id), array(
			'id' => $tumblr_id
		), TRUE );
	}
	
	/*
	*
	*	@param object
	*	@return int
	*/
	private function importPostFromTumblr( $post ){
		$wp_post = array();
		$wp_post['post_content'] = translate_tumblr_post_content( $post );
		$wp_post['post_title'] = (isset($post->title)) ? $post->title : '';
		$wp_post['post_status'] = tumblr_state_to_wp_status( $post->state );
		
		// TODO: Figure out how to handle multi-author blogs.
		//$wp_post['post_author'] = $post->author;
		$wp_post['post_date'] = date( 'Y-m-d H:i:s', $post->timestamp );
		$wp_post['post_date_gmt'] = gmdate( 'Y-m-d H:i:s', $post->timestamp );
		$wp_post['tags_input'] = $post->tags;

		// Remove filtering so we retain audio, video, embeds, etc.
		remove_filter( 'content_save_pre', 'wp_filter_post_kses' );
		
		$wp_id = wp_insert_post( $wp_post );
		add_filter( 'content_save_pre', 'wp_filter_post_kses' );
		
		if( $wp_id ){
			set_post_format( $wp_id, tumblr_post_type_to_wp_post_format($post->type) );
			update_post_meta( $wp_id, '_tumblr_base_hostname', parse_url($post->post_url, PHP_URL_HOST) );
			update_post_meta( $wp_id, '_tumblr_post_id', $post->id );
			update_post_meta( $wp_id, '_tumblr_reblog_key', $post->reblog_key );
			
			if( isset($post->source_url) ){
				update_post_meta( $wp_id, 'tumblr_source_url', $post->source_url );
			}

			// Import media from post types as WordPress attachments.
			if( !empty($post->type) ){
				$wp_subdir_from_post_timestamp = date( 'Y/m', $post->timestamp );
				$wp_upload_dir = wp_upload_dir( $wp_subdir_from_post_timestamp );
				
				if( !is_writable($wp_upload_dir['path']) ){
					$msg = sprintf(
						esc_html__('Your WordPress uploads directory (%s) is not writeable, so Tumblr Crosspostr could not import some media files directly into your Media Library. Media (such as images) will be referenced from their remote source rather than imported and referenced locally.', 'tumblr-crosspostr'),
						$wp_upload_dir['path']
					);
					error_log($msg);
				} else {
					switch( $post->type ){
						case 'photo':
							$this->importPhotosInPost($post, $wp_id);
							break;
						case 'audio':
							$this->importAudioInPost($post, $wp_id);
							break;
						default:
							// TODO: Import media from other post types?
							break;
					}
				}
			}
		}
		return $wp_id;
	}

	/**
 	* Imports some URL-accessible asset (image, audio file, etc.) as a
 	* WordPress attachment and associates it with a given WordPress post.
 	*
 	* @param string $media_url The URL to the asset.
 	* @param object $post Tumblr post object.
 	* @param int $wp_id WordPress post ID number.
 	* @param string $replace_this Content referencing remote asset to replace with locally imported asset.
 	* @param string $replace_with_this_before Content to prepend to locally imported asset URL, such as the start of a shortcode.
 	* @param string $replace_with_this_after Content to append to locally imported asset URL, such as the end of a shortcode.
 	*/
	private function importMedia( $media_url, $post, $wp_id, $replace_this, $replace_with_this_before = '', $replace_with_this_after = '' ){
		$data = wp_remote_get( $media_url, array('timeout' => 300) ); // Download for 5 minutes, tops.
		if( is_wp_error($data) ){
			$msg = sprintf(
				esc_html__('Failed to get Tumblr media (%1$s) from post (%2$s). Server responded: %3$s', 'tumblr-crosspostr'),
				$media_url,
				$post->post_url,
				print_r($data, true)
			);
			
			error_log($msg);
		} else {
			$f = wp_upload_bits( basename($media_url), null, $data['body'], date('Y/m', $post->timestamp) );
				
			if( $f['error'] ){
				$msg = sprintf(
					esc_html__('Error saving file (%s): ', 'tumblr-crosspostr'),
					basename($media_url)
				);
				error_log($msg);
			} else {
				$wp_upload_dir = wp_upload_dir( date('Y/m', $post->timestamp) );
				$wp_filetype = wp_check_filetype( basename($f['file']) );
				$wp_file_id = wp_insert_attachment( array(
					'post_title' => basename( $f['file'], '.'.$wp_filetype['ext'] ),
					'post_content' => '', // Always empty string.
					'post_status' => 'inherit',
					'post_mime_type' => $wp_filetype['type'],
					'guid' => $wp_upload_dir['url'] . '/' . basename($f['file'])
				), $f['file'], $wp_id );
				
				require_once ABSPATH . 'wp-admin/includes/media.php';
				require_once ABSPATH . 'wp-admin/includes/image.php';
				
				$metadata = wp_generate_attachment_metadata( $wp_file_id, $f['file'] );
				wp_update_attachment_metadata( $wp_file_id, $metadata );
				$new_content = str_replace( $replace_this, $replace_with_this_before . $f['url'] . $replace_with_this_after, get_post_field('post_content', $wp_id) );
				
				wp_update_post( array(
					'ID' => $wp_id,
					'post_content' => $new_content
				) );
			}
		}
	}
	
	/*
	*
	*	@param object
	*	@param int
	*	@return
	*/
	private function importPhotosInPost( $post, $wp_id ){
		foreach( $post->photos as $photo){
			$this->importMedia( $photo->original_size->url, $post, $wp_id, $photo->original_size->url );
		}
	}
	
	/*
	*
	*	@param object
	*	@param int
	*	@return
	*/
	private function importAudioInPost( $post, $wp_id ){
		$player_atts = wp_kses_hair( $post->player, array('http', 'https') );
		$x = parse_url( $player_atts['src']['value'], PHP_URL_QUERY );
		$vars = array();
		parse_str( $x, $vars );
		$audio_file_url = urldecode( $vars['audio_file'] );

		$this->importMedia( $audio_file_url, $post, $wp_id, $post->player, '[audio src="', '"]' );
	}
}