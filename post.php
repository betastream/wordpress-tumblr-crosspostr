<?php

namespace sjr\tumblr_poster;

/*
*
*	performs action sjr\tumblr_poster\update_post_id if id is changed
*	@param int
*	@param WP_Post
*	@return
*/
function edit_post( $post_id, $post ){
	if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
		return;

	if( wp_is_post_revision($post_id) )
		return;

	if( !isset($_POST['tumblr_crosspostr_meta_box_nonce']) || !wp_verify_nonce($_POST['tumblr_crosspostr_meta_box_nonce'], 'editing_tumblr_crosspostr') )
		return;

	if( isset($_POST['tumblr_crosspostr_use_excerpt']) ){
		update_post_meta( $post_id, 'tumblr_crosspostr_use_excerpt', 1 );
	} else {
		delete_post_meta( $post_id, 'tumblr_crosspostr_use_excerpt' );
	}

	// update tumblr post id if set
	if( isset($_POST['tumblr_id']) ){
		foreach( $_POST['tumblr_id'] as $old => $new ){
			$new = parse_tumblr_id( $new );
			
			if( $new )
				update_post_meta( $post_id, '_tumblr_post_id', $new, $old );
			else
				delete_post_meta( $post_id, '_tumblr_post_id', $old );

			do_action( 'sjr\tumblr_poster\update_post_id', $new, $old, $post_id );
		}
	}

	// 
	if( !empty($_POST['tumblr_crosspostr_crosspost']) && in_array($_POST['tumblr_crosspostr_crosspost'], array('Y', 'N')) )
		update_post_meta( $post_id, 'tumblr_crosspostr_crosspost', strtoupper($_POST['tumblr_crosspostr_crosspost']) );
}
add_action( 'edit_post', __NAMESPACE__.'\edit_post', 10, 2 );

/*
*
*	@param int
*	@param WP_Post
*	@param bool
*	@return
*/
function save_post( $post_id, $post, $update ){
	if( !\sjr\is_post_real_save($post_id) )
		return;

	// this is to make sure video links work on scheduled / cron posts
	global $wp_embed;
	$wp_embed->cache_oembed( $post_id );

	if( $prepared_post = prepare_for_tumblr($post_id) ){
		$data = tumblr_api_post( $prepared_post );

		if( !empty($data->error) ){
			$msg = render( 'admin/crosspost-fail', array(
				'data' => $data->error
			) );

			set_transient( 'tumblr-crosspost-error', $msg );
			
			add_filter( 'redirect_post_location', __NAMESPACE__.'\redirect_post_location_error', 10, 2 );
		}
	}
}
add_action( 'save_post', __NAMESPACE__.'\save_post', 100, 3 );