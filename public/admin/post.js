jQuery( document ).ready( function($){
	"use strict";

	// edit tumblr post id association
	$( 'a.sjr-tumblr-edit' ).click( function(e){
		e.preventDefault();
		
		var $a = $( this );
		var $div = $a.parent( 'div.sjr-tumblr-action' );
		var $edit = $div.clone( true );
		var $view = $div.prev( 'a' );
		var tumblr_id = $div.data( 'tumblr-id' );
		
		var $input = $( '<input type="text"/>' );
		$input.attr( 'name', 'tumblr_id['+tumblr_id+']' );
		$input.val( tumblr_id );

		// change to not crosspost when ID changes
		$input.change( function(){
			$( '#tumblr-crosspostr-meta-box ul.yn input[value=N]' ).attr( 'checked', true );
		} );

		var $cancel = $( '<a class="sjr-tumblr-cancel">cancel</a>' );
		$cancel.click( function(e){
			e.preventDefault();

			$div.replaceWith( $edit );
			$input.replaceWith( $view );
		} );

		$view.replaceWith( $input );
		$div.html( $cancel );
	} );
} );