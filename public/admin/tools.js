jQuery( document ).ready( function($){
	"use strict";
	
	var $checkboxes = $( 'div#post_types input[type=checkbox]' );
	var $post_dates = $( 'select#tumblr_post_dates' );
	
	$checkboxes.change( function(){
		var data = $( 'div#post_types input' ).serializeArray();
		data.push( {name: 'action', value: 'tumblrize_post_types'} );
		
		$post_dates.attr( 'disabled', 'disabled' );
		
		$.ajax( {
			data: $.param(data),
			success: function( data, textStatus, jqXHR ){
				$post_dates.replaceWith( data );
				$post_dates = $( 'select#tumblr_post_dates' );
			},
			url: ajaxurl
		} );
	} )
} );