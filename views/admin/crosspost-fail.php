<?php if( isset($data->meta) ): ?>
	<p>Remote service said:</p>

	<blockquote>
		Response code: <?php echo esc_html( $data->meta->status ); ?><br/>
		Response message: <?php echo esc_html( $data->meta->msg ); ?>
	</blockquote>

	<?php if( $data->meta->status == 401 ): ?>
		<p>
			This might mean your <a href="<?php echo admin_url( 'options-general.php?page=tumblr_crosspostr_settings' ); ?>">OAuth credentials</a> 
			are invalid or have been revoked by Tumblr. 
		</p>
	<?php endif; ?>
<?php endif; ?>