<p><?php sprintf(
	_n(
		'Success! %1$d post has been crossposted.',
		'Success! %1$d posts have been crossposted to %2$d blogs.',
		$posts_touched,
		'tumblr-crosspostr'
	),
	$posts_touched,
	$blogs_touched
	); ?>
</p>

<p>Blogs touched:</p>
<ul>
<?php foreach( array_unique($blogs) as $blog ): ?>
	<li><a href="<?php echo esc_url( "http://$blog/" ); ?>"><?php echo esc_html( $blog ); ?></a></li>';
<?php endforeach; ?>
</ul>