<h2>Tumblr Crosspostr Settings</h2>

<?php if( !$connected ): ?>
<div class="updated">
	<p>
		Disconnected from Tumblr.
		<span class="description"><?php esc_html_e( 'The connection to Tumblr was disestablished. You can reconnect using the same credentials, or enter different credentials before reconnecting.', 'tumblr-crosspostr' ); ?></span>
	</p>
</div>
<?php endif; ?>

<form method="post" action="options.php">
	<?php settings_fields( 'tumblr_crosspostr_settings' ); ?>
	
	<fieldset>
		<legend><?php esc_html_e( 'Connection to Tumblr', 'tumblr-crosspostr' ); ?></legend>
		
		<table class="form-table" summary="<?php esc_attr_e( 'Required settings to connect to Tumblr.', 'tumblr-crosspostr' ); ?>">
			<tbody>
				<tr<?php if( get_option('tumblr_crosspostr_access_token') ): echo ' style="display: none;"'; endif; ?>>
					<th>
						<label for="tumblr_crosspostr_consumer_key"><?php esc_html_e( 'Tumblr API key/OAuth consumer key', 'tumblr-crosspostr' ); ?></label>
					</th>
					<td>
						<input id="tumblr_crosspostr_consumer_key" name="tumblr_crosspostr_settings[consumer_key]" value="<?php esc_attr_e( $consumer_key ); ?>" placeholder="<?php esc_attr_e( 'Paste your API key here', 'tumblr-crosspostr' ); ?>" />
						<p class="description">
							<?php esc_html_e( 'Your Tumblr API key is also called your consumer key.', 'tumblr-crosspostr' ); ?>
							<?php echo sprintf(
								esc_html__('If you need an API key, you can %s.', 'tumblr-crosspostr'),
								'<a href="' . esc_attr( $tumblr_reg_url ) . '" target="_blank" ' .
								'title="' . __('Get an API key from Tumblr by registering your WordPress blog as a new Tumblr app.', 'tumblr-crosspostr') . '">' .
								__('create one here', 'tumblr-crosspostr') . '</a>'
							); ?>
						</p>
					</td>
				</tr>
				
				<tr<?php if( get_option('tumblr_crosspostr_access_token')) : echo ' style="display: none;"'; endif; ?>>
					<th>
						<label for="tumblr_crosspostr_consumer_secret"><?php esc_html_e( 'OAuth consumer secret', 'tumblr-crosspostr' ); ?></label>
					</th>
					<td>
						<input id="tumblr_crosspostr_consumer_secret" name="tumblr_crosspostr_settings[consumer_secret]" value="<?php esc_attr_e($consumer_secret ); ?>" placeholder="<?php esc_attr_e( 'Paste your consumer secret here', 'tumblr-crosspostr' ); ?>" />
						<p class="description">
							<?php esc_html_e( 'Your consumer secret is like your app password. Never share this with anyone.', 'tumblr-crosspostr' ); ?>
						</p>
					</td>
				</tr>
				<?php if( !get_option('tumblr_crosspostr_access_token') && isset($consumer_key) && isset($consumer_secret) ){ ?>
				<tr>
					<th class="wp-ui-notification" style="border-radius: 5px; padding: 10px;">
						<label for="tumblr_crosspostr_oauth_authorize">Connect to Tumblr:</label>
					</th>
					<td>
						<a href="<?php echo wp_nonce_url( admin_url('options-general.php?page=tumblr_crosspostr_settings&tumblr_crosspostr_oauth_authorize'), 'tumblr-authorize' ); ?>" class="button button-primary">Click here to connect to Tumblr</a>
					</td>
				</tr>
				
				<?php } else if( get_option('tumblr_crosspostr_access_token') ){ ?>
				<tr>
					<th colspan="2">
						<div class="updated">
							<p>
								<?php esc_html_e( 'Connected to Tumblr!', 'tumblr-crosspostr' ); ?>
								<a href="<?php echo wp_nonce_url(admin_url('options-general.php?page=' . 'tumblr_crosspostr_settings&disconnect'), 'disconnect_from_tumblr', 'tumblr_crosspostr_nonce' ); ?>" class="button"><?php esc_html_e( 'Disconnect', 'tumblr-crosspostr' ); ?></a>
								<span class="description"><?php esc_html_e( 'Disconnecting will stop cross-posts from appearing on or being imported from your Tumblr blog(s), and will reset the options below to their defaults. You can re-connect at any time.', 'tumblr-crosspostr' ); ?></span>
							</p>
						</div>
					</th>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</fieldset>
	
<?php if( get_option('tumblr_crosspostr_access_token') ){ ?>
	<fieldset>
		<legend><?php esc_html_e( 'Crossposting Options', 'tumblr-crosspostr' ); ?></legend>
		<table class="form-table" summary="<?php esc_attr_e( 'Options for customizing crossposting behavior.', 'tumblr-crosspostr' ); ?>">
			<tbody>
				<tr<?php if( !$default_hostname) : echo ' class="wp-ui-highlight"'; endif; ?>>
					<th>
						<label for="tumblr_crosspostr_default_hostname"><?php esc_html_e( 'Default Tumblr blog for crossposts', 'tumblr-crosspostr' ); ?></label>
					</th>
					<td>
						<?php echo $selected_blogs; ?>
						<p class="description"><?php esc_html_e( 'Choose which Tumblr blog you want to send your posts to.', 'tumblr-crosspostr' ); ?></p>
					</td>
				</tr>
				
				<!--
				<tr>
					<th>
						<label for="tumblr_crosspostr_sync_from_tumblr"><?php esc_html_e( 'Sync posts from Tumblr', 'tumblr-crosspostr' ); ?></label>
						<p class="description"><?php esc_html_e( '(This feature is experimental. Please backup your website before you turn this on.)', 'tumblr-crosspostr' ); ?></p>
					</th>
					<td>
						<ul id="tumblr_crosspostr_sync_content">
							<?php echo $sync_posts; ?>
						</ul>
						<p class="description"><?php esc_html_e( 'Content you create on the Tumblr blogs you select will automatically be copied to this blog.', 'tumblr-crosspostr' ); ?></p>
					</td>
				</tr>
				-->
				
				<tr>
					<th>
						<label for="tumblr_crosspostr_exclude_categories"><?php esc_html_e( 'Do not crosspost entries in these categories:' ); ?></label>
					</th>
					<td>
						<ul id="tumblr_crosspostr_exclude_categories">
						<?php foreach( get_categories(array('hide_empty' => 0)) as $cat ): ?>
							<li>
								<label>
									<input
										type="checkbox"
										<?php if( $exclude_categories && in_array($cat->slug, $exclude_categories)) : echo 'checked="checked"'; endif; ?>
										value="<?php esc_attr_e($cat->slug); ?>"
										name="tumblr_crosspostr_settings[exclude_categories][]">
									<?php echo esc_html($cat->name); ?>
								</label>
							</li>
						<?php endforeach; ?>
						</ul>
						<p class="description"><?php esc_html_e( 'Will cause posts in the specificied categories never to be crossposted to Tumblr. This is useful if, for instance, you are creating posts automatically using another plugin and wish to avoid a feedback loop of crossposting back and forth from one service to another.', 'tumblr-crosspostr' ); ?></p>
					</td>
				</tr>
		
				<tr>
					<th>
						<label for="tumblr_crosspostr_auto_source_yes"><?php esc_html_e( 'Use permalinks from this blog as the "Content source" for crossposts on Tumblr?' ); ?></label>
					</th>
					<td>
						<ul style="float: left;">
							<li>
								<label>
									<input type="radio" id="tumblr_crosspostr_auto_source_yes"
										name="tumblr_crosspostr_settings[auto_source]"
										<?php if( !$auto_source || $auto_source === 'Y' ){ echo 'checked="checked"'; } ?>
										value="Y" />
									<?php esc_html_e( 'Yes', 'tumblr-crosspostr' ); ?>
								</label>
							</li>
							<li>
								<label>
									<input type="radio" id="tumblr_crosspostr_auto_source_no"
										name="tumblr_crosspostr_settings[auto_source]"
										<?php if( $auto_source === 'N' ){ echo 'checked="checked"'; } ?>
										value="N" />
									<?php esc_html_e( 'No', 'tumblr-crosspostr' ); ?>
								</label>
							</li>
						</ul>
						<p class="description" style="padding: 0 5em;"><?php echo sprintf(esc_html__('When enabled, leaving the %sContent source%s field blank on a given entry will result in setting %sthe "Content source" field on your Tumblr post%s to the permalink of your WordPress post. Useful for providing automatic back-links to your main blog, but turn this off if you "secretly" use Tumblr Crosspostr as the back-end of a publishing platform.', 'tumblr-crosspostr'), '<code>', '</code>', '<a href="http://staff.tumblr.com/post/1059624418/content-attribution">', '</a>' ); ?></p>
					</td>
				</tr>
				
				<tr>
					<th>
						<label for="tumblr_crosspostr_use_excerpt"><?php esc_html_e( 'Send excerpts instead of main content?', 'tumblr-crosspostr' ); ?></label>
					</th>
					<td>
						<input type="checkbox" <?php if( $use_excerpt ) : echo 'checked="checked"'; endif; ?> value="1" id="tumblr_crosspostr_use_excerpt" name="tumblr_crosspostr_settings[use_excerpt]" />
						<label for="tumblr_crosspostr_use_excerpt"><span class="description"><?php esc_html_e( 'When enabled, the excerpts (as opposed to the body) of your WordPress posts will be used as the main content of your Tumblr posts. Useful if you prefer to crosspost summaries instead of the full text of your entires to Tumblr by default. This can be overriden on a per-post basis, too.', 'tumblr-crosspostr' ); ?></span></label>
					</td>
				</tr>
				
				<tr>
					<th>
						<label for="tumblr_crosspostr_exclude_tags"><?php esc_html_e( 'Do not send post tags to Tumblr', 'tumblr-crosspostr' ); ?></label>
					</th>
					<td>
						<input type="checkbox" <?php if( $exclude_tags ): echo 'checked="checked"'; endif; ?> value="1" id="tumblr_crosspostr_exclude_tags" name="tumblr_crosspostr_settings[exclude_tags]" />
						<label for="tumblr_crosspostr_exclude_tags"><span class="description">When enabled, tags on your WordPress posts are not applied to your Tumblr posts. Useful if you maintain different taxonomies on your different sites.</span></label>
					</td>
				</tr>
				<tr>
					<th>
						<label for="tumblr_crosspostr_additional_tags">Automatically add these tags to all crossposts:</label>
					</th>
					<td>
						<input id="tumblr_crosspostr_additional_tags" value="<?php if( $additional_tags ): esc_attr_e(implode(', ', $additional_tags) ); endif; ?>" name="tumblr_crosspostr_settings[additional_tags]" placeholder="<?php esc_attr_e( 'crosspost, magic', 'tumblr-crosspostr' ); ?>" />
						<p class="description"><?php echo sprintf(esc_html__('Comma-separated list of additional tags that will be added to every post sent to Tumblr. Useful if only some posts on your Tumblr blog are cross-posted and you want to know which of your Tumblr posts were generated by this plugin. (These tags will always be applied regardless of the value of the "%s" option.)', 'tumblr-crosspostr'), esc_html__('Do not send post tags to Tumblr', 'tumblr-crosspostr') ); ?></p>
					</td>
				</tr>
				
				<tr>
					<th>
						<label for="tumblr_crosspostr_debug">Enable detailed debugging information?</label>
					</th>
					<td>
						<input type="checkbox" <?php if( $debug ) : echo 'checked="checked"'; endif; ?> value="1" id="tumblr_crosspostr_debug" name="tumblr_crosspostr_settings[debug]" />
						<label for="tumblr_crosspostr_debug"><span class="description"><?php
				echo sprintf(
					esc_html__('Turn this on only if you are experiencing problems using this plugin, or if you were told to do so by someone helping you fix a problem (or if you really know what you are doing). When enabled, extremely detailed technical information is displayed as a WordPress admin notice when you take actions like sending a crosspost. If you have also enabled WordPress\'s built-in debugging (%1$s) and debug log (%2$s) feature, additional information will be sent to a log file (%3$s). This file may contain sensitive information, so turn this off and erase the debug log file when you have resolved the issue.', 'tumblr-crosspostr'),
					'<a href="https://codex.wordpress.org/Debugging_in_WordPress#WP_DEBUG"><code>WP_DEBUG</code></a>',
					'<a href="https://codex.wordpress.org/Debugging_in_WordPress#WP_DEBUG_LOG"><code>WP_DEBUG_LOG</code></a>',
					'<code>' . content_url() . '/debug.log' . '</code>'
				);
						?></span></label>
					</td>
				</tr>
				
				<tr>
					<th>
						<label for="">Redirection</label>
					</th>
					<td>
						<ul>
							<li>
								<label><input type="checkbox" name="tumblr_crosspostr_settings[redirect_permalinks]" value="1" <?php echo checked( $redirect_permalinks ); ?>/>
								Replace permalinks with tumblr links</label>
							</li>
							
							<li>
								<label><input type="checkbox" name="tumblr_crosspostr_settings[redirect_on_page]" value="1" <?php echo checked( $redirect_on_page ); ?>/>
								Redirect on post pages to tumblr</label>
							</li>
						</ul>
					</td>
				</tr>
			</tbody>
		</table>
	</fieldset>
		<?php } ?>
		
		<?php submit_button(); ?>
</form>