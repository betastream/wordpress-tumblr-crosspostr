<p>You can automatically copy this post to your Tumblr blog:</p>
<ol>
	<li>
		Compose your post for WordPress as you normally would, with the appropriate <a href="#formatdiv">Post Format</a>.
	</li>
	
	<li>
		In <a href="#tumblr-crosspostr-meta-box">the Tumblr Crosspostr box</a>, ensure the "Send this post to Tumblr?" option is set to "Yes." (You can set it to "No" if you do not want to copy this post to Tumblr.
	</li>
	
	<li>
		If you have more than one Tumblr, choose the one you want to send this post to from the "Send to my Tumblr blog" list.
	</li>
	
	<li>
		Optionally, enter any additional details specifically for Tumblr, such as the "Content source" field.
	</li>
</ol>

<p>When you are done, click "Publish" (or "Save Draft"), and Tumblr Crosspostr will send your post to the Tumblr blog you chose.</p>
<p>Note that Tumblr does not allow you to change the post format after you have saved a copy of your post, so please be sure you choose the appropriate Post Format before you save your post.</p>