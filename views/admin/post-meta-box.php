<?php wp_nonce_field( 'editing_tumblr_crosspostr', 'tumblr_crosspostr_meta_box_nonce' ); ?>

<?php if( $tumblr_posts ): ?>
<fieldset>
	<ul>
		<?php foreach( $tumblr_posts as $tumblr_post ): ?>
		<li>
			<a target="_blank" class="sjr-tumblr-link" href="<?php echo esc_attr( $tumblr_post->url ); ?>" title="View Post on Tumblr"><?php echo esc_attr( $tumblr_post->url ); ?></a>
			
			<?php /*
			<div class="edit">
				<input type="text">
				<a href="">cancel</a></div>
			</div>
			*/ ?>

			<div class="sjr-tumblr-action" data-tumblr-id="<?php echo $tumblr_post->tumblr_id; ?>" data-wp-post-id="<?php echo $tumblr_post->wp_post_id; ?>">
				<a href="#" class="sjr-tumblr-edit" data-nonce="<?php echo wp_create_nonce( 'sjr-tumblr-edit-'.$tumblr_post->tumblr_id ); ?>">edit</a>
			</div>
		</li>
		<?php endforeach; ?>
	</ul>
</fieldset>
<?php endif; ?>

<fieldset>
	<legend style="display:block;"><?php echo count( $tumblr_posts ) ? 'Update' : 'Publish'; ?> this post on Tumblr?</legend>
	<ul class="yn">
		<li>
		<label>
			<input type="radio" name="tumblr_crosspostr_crosspost" value="Y"<?php checked( $send_to_tumblr, 'Y' ); ?>> Yes
		</label></li>
		<li>
			<label>
				<input type="radio" name="tumblr_crosspostr_crosspost" value="N"<?php checked( $send_to_tumblr, 'N' ); ?>> No
			</label>
		</li>
	</ul>
</fieldset>

<?php if( $tumblr_posts && !empty($api_response->id) ): ?>
<fieldset>
	<legend style="display:block;">Tumblr meta</legend>
	<ul>
		<li>Date: <?php echo $api_response->date; ?></li>
	</ul>
</fieldset>
<?php elseif( !empty($api_response->error) ): ?>
	Error: <?php echo $api_response->error; ?>
<?php endif; ?>