<h2>Crosspost Archives to Tumblr</h2>

<p>If you have post archives on this website, Tumblr Crosspostr can copy them to your Tumblr blog.</p>

<form action="tools.php">
	<input type="hidden" name="page" value="tumblr_crosspostr_crosspost_archives">
	
	<div id="post_types">
	<?php foreach( $post_types as $post_type ): ?>
		<label><input type="checkbox" name="post_types[<?php echo $post_type->name; ?>]" value="<?php echo $post_type->name; ?>"/><?php echo $post_type->labels->name; ?></label>
	<?php endforeach; ?>
	</div>
	
	<fieldset>
		<p>Export by month</p>
		<?php echo $date_select; ?>
		
		<button type="submit" name="export_by_month" value="<?php echo wp_create_nonce( 'export_by_month' ); ?>">Export</button>
	</fieldset>
	
	<fieldset>
		<p>Comma separated post IDs</p>
		<textarea name="export_ids"></textarea>
		<button type="submit" name="export_by_ids" value="<?php echo wp_create_nonce( 'export_by_ids' ); ?>">Export Post IDs</button>
	</fieldset>
	
	<fieldset>
		<p>Export all posts</p>
		<button type="submit" name="export_all" disabled="disabled" value="<?php echo wp_create_nonce( 'export_all' ); ?>">Export All</button>
	</fieldset>
</form>

<p class="description"><?php echo sprintf( esc_html__('Copies all posts from your archives to your default Tumblr blog (%s). This may take some time if you have a lot of content. If you do not want to crosspost a specific post, set the answer to the "Send this post to Tumblr?" question to "No" when editing those posts before taking this action. If you have previously crossposted some posts, this will update that content on your Tumblr blog(s).', 'tumblr-crosspostr'), '<code>' . esc_html( $default_hostname ) . '</code>' ); ?></p>